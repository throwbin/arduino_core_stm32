# CMake generated Testfile for 
# Source directory: /home/pi/stlink/stlink/tests
# Build directory: /home/pi/Arduino/hardware/stm/tools/linux/stlink/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(usb "/home/pi/Arduino/hardware/stm/tools/linux/stlink/tests/usb")
add_test(sg "/home/pi/Arduino/hardware/stm/tools/linux/stlink/tests/sg")
add_test(flash "/home/pi/Arduino/hardware/stm/tools/linux/stlink/tests/flash")
