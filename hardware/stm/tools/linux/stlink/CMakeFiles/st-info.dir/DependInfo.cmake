# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/stlink/stlink/src/tools/info.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/st-info.dir/src/tools/info.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CMAKE_SOURCE_DIR_LENGTH=23"
  "STLINK_HAVE_SYS_MMAN_H"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/libusb-1.0"
  "/home/pi/stlink/stlink/include"
  "include"
  "/home/pi/stlink/stlink/src/mingw"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-shared.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
