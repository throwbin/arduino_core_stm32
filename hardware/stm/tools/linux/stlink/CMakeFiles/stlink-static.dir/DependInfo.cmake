# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/stlink/stlink/src/chipid.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/chipid.c.o"
  "/home/pi/stlink/stlink/src/common.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/common.c.o"
  "/home/pi/stlink/stlink/src/flash_loader.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/flash_loader.c.o"
  "/home/pi/stlink/stlink/src/logging.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/logging.c.o"
  "/home/pi/stlink/stlink/src/sg.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/sg.c.o"
  "/home/pi/stlink/stlink/src/usb.c" "/home/pi/Arduino/hardware/stm/tools/linux/stlink/CMakeFiles/stlink-static.dir/src/usb.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CMAKE_SOURCE_DIR_LENGTH=23"
  "STLINK_HAVE_SYS_MMAN_H"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/libusb-1.0"
  "/home/pi/stlink/stlink/include"
  "include"
  "/home/pi/stlink/stlink/src/mingw"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
