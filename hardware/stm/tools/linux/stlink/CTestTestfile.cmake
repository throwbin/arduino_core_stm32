# CMake generated Testfile for 
# Source directory: /home/pi/stlink/stlink
# Build directory: /home/pi/Arduino/hardware/stm/tools/linux/stlink
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src/gdbserver)
subdirs(src/tools/gui)
subdirs(usr/lib/pkgconfig)
subdirs(include)
subdirs(doc/man)
subdirs(tests)
